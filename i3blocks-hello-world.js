#!/usr/bin/env node
;(function () {
  'use strict'

  var env = process.env
  var keys = Object.keys(env).sort()

  var blockVars = keys.reduce(function (string, key) {
    if (/^BLOCK/.test(key)) {
      string += key.replace(/^BLOCK_/, '') + ': ' + env[key] + ' '
    }

    return string
  }, '')

  console.log(blockVars)
})()

